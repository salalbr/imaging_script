#!/bin/sh
##############################################################################
#  imaging_script
#  Authors:
#              Maer Melo | mmelo@novell.com
##############################################################################

model=$(dmidecode -s "system-product-name")
echo $model

case "$model" in
	*6550b*)
		echo "::Started deploying 6550b" ;
		img -rp addon-image/732-drivers-6x50b.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 6550b" ;;
	*8000*)
		echo "::Start deploying 8000" ;
		img -rp addon-image/732-drivers-8000.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 8000" ;;
	*8200*)
		echo "::Start deploying 8200" ;
		img -rp addon-image/732-drivers-8200.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 8200" ;;
	*6560b*)
		echo "::Start deploying 6560b" ;
		img -rp addon-image/732-drivers-6x60b.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 6560b" ;;
	*6570b*)
		echo "::Start deploying 6570b" ;
		img -rp addon-image/732-drivers-6x70b.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 6570b" ;;
	*8760w*)
		echo "::Start deploying 8760w" ;
		img -rp addon-image/732-drivers-8x60.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 8760w" ;;
	*dc7900*)
		echo "::Start deploying dc7900" ;
		img -rp addon-image/732-drivers-7900.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying dc7900" ;;
	*8300*)
		echo "::Start deploying 8300" ;
		img -rp addon-image/732-drivers-8300.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying 8300" ;;
	*z400*)
		echo "::Start deploying z400" ;
		img -rp addon-image/732-drivers-z400.zmg -ip=$PROXYADDR ;
		echo "::Finished deploying z400" ;;
	*VMware*)
		echo "VMware No Work To Do!" ;
		echo "::Finished deploying VMware" ;;
esac
